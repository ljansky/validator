'use strict';

export default function sameAs (object, config) {
	const errors = {};
	const value = object[config.field];
	const valueToCompare = object[config.compareTo];

	if (value !== valueToCompare) {
		errors[config.field] = config.message || config.type;
	}
    
	return errors;
}