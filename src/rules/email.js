'use strict';

export default function email(object, config) {
	const errors = {};
	const re = /\S+@\S+\.\S+/;
	const value = object[config.field];
	if (value && value !== '' && !re.test(value)) {
		errors[config.field] = config.message || config.type;
	}

	return errors;
}