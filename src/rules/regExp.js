'use strict';

export default function regExp(object, config) {
	const errors = {};
	const reg = new RegExp(config.regExp);

	const value = object[config.field];
	if (value && value !== '' && !reg.test(value)) {
		errors[config.field] = config.message || config.type;
	}

	return errors;
}